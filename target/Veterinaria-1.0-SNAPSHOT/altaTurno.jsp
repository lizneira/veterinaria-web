<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/estilo.css" type="text/css"/>
        <title>Alta Turno</title>
    </head>
    <body>
        <h1 class="registro-titulo">Alta Turno</h1>
        <form class="registro" id="form-one" method="post" action="ControladorAltaTurno">
            <input type="text" class="register-input" name="nombreDuenio" placeholder="Nombre" required>
            <input type="number" class="register-input" name="numeroTelefono" placeholder="Número telefónico" required>
            <input type="text" class="register-input" name="nombreMascota" placeholder="Nombre de la mascota" required>
            <input type="time" class="register-input" name="tiempo" required>
            <select class="register-select register-input" name="tipo">
                <option class="registro-option" value="tortuga">Tortuga</option>
                <option class="registro-option" value="gato">Gato</option>
                <option class="registro-option" value="perro">Perro</option>
                <option class="registro-option" value="canario">Canario</option>
            </select>
            <button type="submit" class="registro-button" form="form-one" name="estado" value="espera">Registrar</button>
        </form>
        <button class="boton boton-atras" onclick="location.href='recepcionista.jsp'" type="button">Regresar</button><br>
        <c:if test="${not empty mensaje}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${mensaje}");
            </script>
        </c:if> 
    </body>
</html>
