<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/estilo.css" type="text/css"/>
        <title>Medicamentos Disponible </title>
    </head>
    <body>
        <c:choose>
            <c:when test="${empty productos}">
                <div class="div-title-page">
                    <h1 class="h1-title-page">NO HAY PRODUCTOS DISPONIBLES</h1>
                </div>
            </c:when>
            <c:otherwise>
                <div class="container">
                    <table>
                        <thead>
                            <tr>
				<th>ID</th>
				<th>Descripción</th>
				<th>Tipo</th>
				<th>Precio</th>
				<th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${productos}" var="producto">
                                <tr>
                                    <td>${producto.productoID}</td>
                                    <td>${producto.descripcion}</td>
                                    <td>${producto.tipo}</td>
                                    <td>${producto.precio}</td>
                                    <td>${producto.estado}</td>
                                </tr>
                             </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
        <button class="boton boton-atras" onclick="location.href='administrador.jsp'" type="button">Regresar</button><br>
    </body>
</html>
