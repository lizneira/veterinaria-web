
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/index.css" type="text/css"/>
        <title>Veterinaria</title>
    </head>
    <body class="home-index">

        <div class="usuario">
            <form id="formulario" class="form" method="post" action="ControladorRegistro">
                <div>
                    <input id="titulo" name="rolEmpleado" type="text" value="Administrador" readonly="readonly" tabindex="-1"/>
                </div>
                <div>
                    <input class="form-input" name="nombreEmpleado" type="text" placeholder="Usuario" required />
                </div>
                <div>
                    <input class="form-input"  name="contraseniaEmpleado" type="password" placeholder="Contraseña" required />
                </div>
                <button class="button" type="submit" form="formulario" name="sender" value="system">Registrar</button>
        </form>
        </div>
    </body>
</html>
