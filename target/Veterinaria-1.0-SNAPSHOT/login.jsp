<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/login.css" type="text/css"/>
        <title>Login</title>
    </head>
    <body>
        <div class="login">
            <h1>Iniciar Sesión</h1>
            <form id="form-one" method="post" action="ControladorLogin">
                <input type="text" name="nombreEmpleado" placeholder="Usuario" required="required" />
                <input type="password" name="contraseniaEmpleado" placeholder="Contraseña" required="required" />
                <button type="submit" class="btn btn-primary btn-block btn-large" form="form-one">Ingresar</button>
            </form>
        </div>
        <c:if test="${not empty mensaje}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${mensaje}");
            </script>
        </c:if>
    </body>
</html>
