<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/login.css" type="text/css"/>
        <title>Recepcionista</title>
    </head>
    <body class="home-recepcionista">
        <button class="button sign-out-button" onclick="location.href='login.jsp'" type="button">Cerrar Sesión</button><br>
         <div class="div-titulo-pagina">
            <h1 class="h1-titulo-pagina">RECEPCIÓN</h1>
        </div>
        <div class="main">
            <div class="secondary">
                <form id="formulario" method="post" action="ControladorListaProductosVendidos">
                    <button class="button" type="submit" name="tipoProducto" value="regular" form="formulario">Vender regulares</button>
                    <input type="hidden" name="sender" value="recepcionista">
                </form>
                <button class="button" onclick="location.href='altaTurno.jsp'" type="button">Agendar turnos</button><br>
            </div>
        </div>
        </body>
    
</html>
