<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/estilo.css" type="text/css"/>
        <title>Vender Producto</title>
    </head>
    <body>
        <form id="form-one" method="post" action="ControladorVenderProducto"> 
            <c:choose>
            <c:when test="${empty productos}">
                <div class="div-title-page">
                    <h1 class="h1-title-page">NO HAY PRODUCTOS PARA VENDER</h1>
                </div>
            </c:when>
            <c:otherwise>
                <div class="div-title-page">
                    <h1 class="h1-title-page">PRODUCTOS DISPONIBLES</h1>
                </div>
                <div class="container">
                    <table>
                        <thead>
                            <tr>
				<th></th>
				<th>Descripción</th>
				<th>Tipo</th>
				<th>Precio</th>
				<th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${productos}" var="producto">
                                <tr>
                                    <td><input type="checkbox" name="productos" value="${producto.productoID}"></td>
                                    <td>${producto.descripcion}</td>
                                    <td><input type="hidden" name="tipoProducto" value="${producto.tipo}"/>${producto.tipo}</td>
                                    <td>${producto.precio}</td>
                                    <td><input type="hidden" name="disponible" value="${producto.estado}"/>${producto.estado}</td>
                                </tr>
                             </c:forEach>
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="action-button" name="estado" value="vendido" form="form-one">Vender</button>
                <input type="hidden" name="volverPagina" value="${volverPagina}"/>
            </c:otherwise>
        </c:choose>
        </form>	
        <button class="button back-button" onclick="location.href='${volverPagina}'" type="button">Regresar</button><br>
        <c:if test="${!mensaje.equals('noHayProductos') && mensaje != null}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${mensaje}");
            </script>
        </c:if>
    </body>
</html>
