<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/estilo.css" type="text/css"/>
        <title>Alta Producto</title>
    </head>
    <body>
        <h1 class="registro-titulo">Registrar Productos</h1>
        <form class="registro" id="form-one" method="post" action="ControladorAltaProducto">
          <div class="register-switch">
            <input type="radio" name="tipo" value="medicamento" id="medication" class="register-switch-input"checked required>
            <label for="medicacion" class="register-switch-label">Medicamento</label>
            <input type="radio" name="tipo" value="regular" id="regular" class="register-switch-input"required>
            <label for="regular" class="register-switch-label">Regular</label>
          </div>
          <input type="text" class="register-input" name="descripcion" placeholder="Descripción" required>
          <input type="number" class="register-input" name="precio" placeholder="Precio" required>
          <button type="submit" class="register-button" form="form-one" name="estado" value="disponible">Registrar</button>
        </form>
        <button class="boton boton-atras" onclick="location.href='administrador.jsp'" type="button">Regresar</button><br>
         <c:if test="${not empty mensaje}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${mensaje}");
            </script>
        </c:if>
    </body>
</html>
