<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/estilo.css" type="text/css"/>
        <title>Lista Turnos</title>
    </head>
    <body>
        <form id="form-one" method="post" action="ControladorAtenderMascota"> 
            <c:choose>
                <c:when test="${empty turnos}">
                    <div class="div-titulo-pagina">
                        <h1 class="h1-titulo-pagina">NO HAY TURNOS</h1>
                    </div>
                </c:when>
                <c:otherwise> 
                    <div class="div-titulo-pagina">
                        <h1 class="h1-titulo-pagina">TURNOS POR ATENDER</h1>
                    </div>
                    <div class="container">
                        <table>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nombre</th>
                                    <th>Número telefónico</th>
                                    <th>Nombre de la mascota</th>
                                    <th>Tipo de mascota</th>
                                    <th>Hora</th>
                                    <th>Estado del turno</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${turnos}" var="turno">
                                    <tr>
                                        <td><input type="checkbox" name="turnos" value="${turno.turnoID}"></td>
                                        <td>${turno.nombreDuenio}</td>
                                        <td>${turno.numeroTelefono}</td>
                                        <td>${turno.nombreMascota}</td>
                                        <td>${turno.tipo}</td>
                                        <td>${turno.tiempo}</td>
                                        <td>${turno.estado}</td>
                                    </tr>
                                 </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="action-button" name="estado" value="atendido" form="form-one">Atender</button>      
                </c:otherwise>
            </c:choose>
        </form>	
        <button class="boton boton-atras" onclick="location.href='veterinario.jsp'" type="button">Regresar</button><br>
        <c:if test="${!mensaje.equals('no hay turnos') && mensaje != null}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${mensaje}");
            </script>
        </c:if>
    </body>
</html>
