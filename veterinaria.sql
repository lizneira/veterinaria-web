-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.22-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para veterinaria
DROP DATABASE IF EXISTS `veterinaria`;
CREATE DATABASE IF NOT EXISTS `veterinaria` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `veterinaria`;

-- Volcando estructura para tabla veterinaria.empleados
DROP TABLE IF EXISTS `empleados`;
CREATE TABLE IF NOT EXISTS `empleados` (
  `empleadoID` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEmpleado` varchar(30) CHARACTER SET utf8 NOT NULL,
  `contraseniaEmpleado` varchar(30) CHARACTER SET utf8 NOT NULL,
  `rolEmpleado` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`empleadoID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla veterinaria.productos
DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `productoID` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(30) CHARACTER SET utf8 NOT NULL,
  `precio` int(11) NOT NULL,
  `estado` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`productoID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla veterinaria.turnos
DROP TABLE IF EXISTS `turnos`;
CREATE TABLE IF NOT EXISTS `turnos` (
  `tunoID` int(11) NOT NULL AUTO_INCREMENT,
  `nombreDuenio` varchar(30) CHARACTER SET utf8 NOT NULL,
  `numeroTelefono` int(11) NOT NULL,
  `nombreMascota` varchar(30) CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(30) CHARACTER SET utf8 NOT NULL,
  `tiempo` time NOT NULL,
  `estado` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`tunoID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
