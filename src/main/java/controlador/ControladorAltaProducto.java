package controlador;

import utils.AutUtil;
import utils.RutaPaginaUtil;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Session;

@WebServlet(name = "ControladorAltaProducto", urlPatterns = {"/ControladorAltaProducto"})
public class ControladorAltaProducto extends HttpServlet {
    
    private Producto producto = null;
    private AutUtil autUtil = null;
    private RutaPaginaUtil rutaPaginaUtil = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        producto = new Producto();
        autUtil = new AutUtil();
        rutaPaginaUtil = new RutaPaginaUtil();
        RequestDispatcher rd;
        String pagina;
        String descripcion = request.getParameter("descripcion");
        int precio = Integer.parseInt(request.getParameter("precio"));
        String tipo = request.getParameter("tipo");
        String estado = request.getParameter("estado");

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            boolean isRegisteredProduct = autUtil.hayProducto(session, descripcion, tipo, precio, estado);
            if (isRegisteredProduct) {
                pagina = rutaPaginaUtil.getRutaPagina("altaProducto");
                request.setAttribute("mensaje", "El producto ya existe");
            } else {
                producto.setDescripcion(descripcion);
                producto.setTipo(tipo);
                producto.setPrecio(precio);
                producto.setEstado(estado);
                session.save(producto); 
                pagina = rutaPaginaUtil.getRutaPagina("altaProducto");
                request.setAttribute("mensaje", "El producto fue ingresado");
            }
            session.getTransaction().commit();
            session.close();  
        }        
        rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
