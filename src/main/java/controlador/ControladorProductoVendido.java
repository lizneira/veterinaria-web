package controlador;

import utils.RutaPaginaUtil;
import utils.ProductoUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Session;

public class ControladorProductoVendido extends HttpServlet {

    private RutaPaginaUtil rutaPaginaUtil = null;
    private ProductoUtil productoUtil = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        rutaPaginaUtil = new RutaPaginaUtil();
        productoUtil = new ProductoUtil();
        String pagina;
        String estado = "vendido";
        String tipoProducto = request.getParameter("tipoProducto");
        if(tipoProducto.equals("regular")) {
            request.setAttribute("producto", "Regulares");
        } else {
            request.setAttribute("producto", "Medicaciones");
        }
       
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
           
            List <Producto> productos = productoUtil.getProductos(session, tipoProducto, estado);
            if (productos.isEmpty()) {
                request.setAttribute("mensaje", "No hay productos");
            } else {
                request.setAttribute("productos", productos);
            }
            pagina = rutaPaginaUtil.getRutaPagina("productoVendido");
            session.getTransaction().commit();
            session.close();  
        }       
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
