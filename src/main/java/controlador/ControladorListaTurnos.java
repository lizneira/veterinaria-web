package controlador;

import utils.TurnoUtil;
import utils.RutaPaginaUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Turno;
import modelo.HibernateUtil;
import org.hibernate.Session;

@WebServlet(name = "ControladorListaTurnos", urlPatterns = {"/ControladorListaTurnos"})
public class ControladorListaTurnos extends HttpServlet {

    private RutaPaginaUtil rutaPaginaUtil = null;
    private TurnoUtil turnoUtil = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        rutaPaginaUtil = new RutaPaginaUtil();
        turnoUtil = new TurnoUtil();
        String pagina;
        String estado = request.getParameter("estado");
       
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            List <Turno> turnos = turnoUtil.getTurnos(session, estado);
            if (turnos.isEmpty()) {
                request.setAttribute("mensaje", "no hay turno");
            } else {
                request.setAttribute("turnos", turnos);
            }
            pagina = rutaPaginaUtil.getRutaPagina("listaTurno");
            session.getTransaction().commit();
            session.close();  
        }       
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
