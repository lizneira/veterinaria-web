package controlador;

import utils.AutUtil;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Empleado;
import modelo.HibernateUtil;
import org.hibernate.Session;
import utils.RutaPaginaUtil;
import java.util.List;

@WebServlet(name = "ControladorRegistro", urlPatterns = {"/ControladorRegistro"})
public class ControladorRegistro extends HttpServlet {

    private Empleado empleado = null;
    private RutaPaginaUtil rutaPaginaUtil = null;
    private AutUtil autUtil = null;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        empleado = new Empleado();
        rutaPaginaUtil = new RutaPaginaUtil();
        autUtil = new AutUtil();
        boolean estado = false;
        String pagina;
        String nombreEmpleado = request.getParameter("nombreEmpleado");
        String contraseniaEmpleado= request.getParameter("contraseniaEmpleado");
        String rolEmpleado = request.getParameter("rolEmpleado").toLowerCase();
        String sender = request.getParameter("sender");
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            List<Empleado> empleados = autUtil.getEmpleado(session, nombreEmpleado, contraseniaEmpleado);
            if (empleados.size() > 0) {
                estado = true;
                request.setAttribute("mensaje", "El usuario ya existe");
                pagina = rutaPaginaUtil.getRutaPagina("index");
            } else {
                empleado.setNombreEmpleado(nombreEmpleado);
                empleado.setContraseniaEmpleado(contraseniaEmpleado);
                empleado.setRolEmpleado(rolEmpleado);
                session.save(empleado); 
                request.setAttribute("mensaje", "El usuario fue registrado");
            }
            switch(sender) {
                case "administrador":
                    pagina = rutaPaginaUtil.getRutaPagina("altaEmpleado");
                    break;
                default:
                    if(estado) {
                        pagina = rutaPaginaUtil.getRutaPagina("index");    
                    } else {
                        pagina = rutaPaginaUtil.getRutaPagina("login");
                    }
                    break;
            }
            session.getTransaction().commit();
            session.close();  
        }
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
