package controlador;

import utils.RutaPaginaUtil;
import utils.ProductoUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Session;

@WebServlet(name = "ControladorListaProductosVendidos", urlPatterns = {"/ControladorListaProductosVendidos"})
public class ControladorListaProductosVendidos extends HttpServlet {

    private RutaPaginaUtil rutaPaginaUtil = null;
    private ProductoUtil productoUtil = null;

   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        rutaPaginaUtil = new RutaPaginaUtil();
        productoUtil = new ProductoUtil();
        String pagina;
        String estado = "disponible";
        String tipoProducto = request.getParameter("tipoProducto");
        String volverPagina = request.getParameter("sender").equals("veterinario") ? rutaPaginaUtil.getRutaPagina("veterinario") : rutaPaginaUtil.getRutaPagina("recepcionista");
        request.setAttribute("volverPagina", volverPagina);
        if (tipoProducto.equals("regular")) {
            request.setAttribute("producto", "Regulares");
        } else {
            request.setAttribute("producto", "Medicaciones");
        }

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            List<Producto> productos = productoUtil.getProductos(session, tipoProducto, estado);
            if (productos.isEmpty()) {
                request.setAttribute("mensaje", "noHayProductos");
            } else {
                request.setAttribute("productos", productos);
            }
            pagina = rutaPaginaUtil.getRutaPagina("venderProducto");
            session.getTransaction().commit();
            session.close();
        }
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
