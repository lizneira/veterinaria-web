package controlador;

import utils.RutaPaginaUtil;
import utils.ProductoUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Session;

@WebServlet(name = "ControladorVenderProducto", urlPatterns = {"/ControladorVenderProducto"})
public class ControladorVenderProducto extends HttpServlet {
    
    private RutaPaginaUtil rutaPaginaUtil = null;
    private ProductoUtil productoUtil = null;
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String [] productoIDs = request.getParameterValues("productos");
        String tipoProducto = request.getParameter("tipoProducto");
        String estado = request.getParameter("estado");
        String volverPagina = request.getParameter("volverPagina");
        rutaPaginaUtil = new RutaPaginaUtil();
        String pagina;
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            productoUtil = new ProductoUtil();
            int numeroRegistroEliminado = productoUtil.eliminarProducto(session, productoIDs, estado);
            if (numeroRegistroEliminado > 0) {
                request.setAttribute("mensaje", numeroRegistroEliminado +" producto(s) vendido(s)");
            }
            estado = "disponible";
            List <Producto> productos = productoUtil.getProductos(session, tipoProducto, estado);
             if (!productos.isEmpty()) {
                request.setAttribute("productos", productos);
            } 
            pagina = rutaPaginaUtil.getRutaPagina("venderProducto");
            session.getTransaction().commit();
            session.close();  
        }       
        request.setAttribute("volverPagina", volverPagina);
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
