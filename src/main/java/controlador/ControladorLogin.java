package controlador;

import utils.AutUtil;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.HibernateUtil;
import org.hibernate.Session;
import utils.RutaPaginaUtil;
import java.util.List;
import modelo.Empleado;

@WebServlet(name = "ControladorLogin", urlPatterns = {"/ControladorLogin"})
public class  ControladorLogin extends HttpServlet {
    
    private RutaPaginaUtil rutaPaginaUtil = null;
    private AutUtil autUtil = null;
  
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        rutaPaginaUtil = new RutaPaginaUtil();
        autUtil = new AutUtil();
        String nombreEmpleado = request.getParameter("nombreEmpleado");
        String contraseniaEmpleado = request.getParameter("contraseniaEmpleado");
        String pagina;
        String rolEmpleado;
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            List<Empleado> empleados = autUtil.getEmpleado(session, nombreEmpleado, contraseniaEmpleado);
            if (empleados.size() > 0) {
                rolEmpleado = empleados.get(0).getRolEmpleado();
                pagina = rutaPaginaUtil.getRutaPagina(rolEmpleado);
            } else {
                pagina = rutaPaginaUtil.getRutaPagina("login");
                request.setAttribute("mensaje", "El usuario no existe");
            }
            session.getTransaction().commit();
            session.close();
        }       
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}

