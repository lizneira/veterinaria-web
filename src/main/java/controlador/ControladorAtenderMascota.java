package controlador;

import utils.TurnoUtil;
import utils.RutaPaginaUtil;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Turno;
import modelo.HibernateUtil;
import org.hibernate.Session;

@WebServlet(name = "ControladorAtenderMascota", urlPatterns = {"/ControladorAtenderMascota"})
public class ControladorAtenderMascota extends HttpServlet {

    private RutaPaginaUtil rutaPaginaUtil = null;
    private TurnoUtil turnoUtil = null;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
        String [] turnoIDs = request.getParameterValues("turnos");
        String estado = request.getParameter("estado");
        rutaPaginaUtil = new RutaPaginaUtil();
        String pagina;
        
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            session.beginTransaction();
            if(turnoIDs != null) {
                turnoUtil = new TurnoUtil();
                int numeroRegistroEliminado = turnoUtil.eliminarTurno(session, turnoIDs, estado);
                request.setAttribute("mensaje", numeroRegistroEliminado +" turno(s) atendido(s)");
                estado = "espera";
                List <Turno> turnos = turnoUtil.getTurnos(session, estado);
                if (!turnos.isEmpty()) {
                    request.setAttribute("turnos", turnos);
                } 
            } else {
                estado = "espera";
                List <Turno> turnos = turnoUtil.getTurnos(session, estado);
                if (!turnos.isEmpty()) {
                    request.setAttribute("turnos", turnos);
                }
            }
            pagina = rutaPaginaUtil.getRutaPagina("listaTurno");
            session.getTransaction().commit();
            session.close();  
        }       
        RequestDispatcher rd = request.getRequestDispatcher(pagina);
        rd.forward(request, response);
    }
}
