package controlador;

import utils.TurnoUtil;
import utils.RutaPaginaUtil;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Turno;
import modelo.HibernateUtil;
import org.hibernate.Session;

@WebServlet(name = "ControladorAltaTurno", urlPatterns = {"/ControladorAltaTurno"})
public class ControladorAltaTurno extends HttpServlet {

    Turno turno = null;
    RutaPaginaUtil rutaPaginaUtil = null;
    TurnoUtil turnoUtil = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        try {
            turno = new Turno();
            rutaPaginaUtil = new RutaPaginaUtil();
            turnoUtil = new TurnoUtil(); 
            String pagina;
            String nombreDuenio = request.getParameter("nombreDuenio");
            int numeroTelefono = Integer.parseInt(request.getParameter("numeroTelefono"));
            String nombreMascota = request.getParameter("nombreMascota");
            String tipo = request.getParameter("tipo");
            String tiempo = request.getParameter("tiempo");
            String estado = request.getParameter("estado");
            Date dateTime;
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            dateTime = dateFormat.parse(tiempo);       

            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                session.beginTransaction();
                String mensaje = turnoUtil.obtenerMensajeValidacionTiempo(tiempo);
                boolean resultado = turnoUtil.tiempoDisponible(session, tiempo);
                if(mensaje.equalsIgnoreCase("ok")) {
                    if(resultado) {
                        turno.setNombreDuenio(nombreDuenio);
                        turno.setNumeroTelefono(numeroTelefono);
                        turno.setNombreMascota(nombreMascota);
                        turno.setTipo(tipo);
                        turno.setTiempo(dateTime);
                        turno.setEstado(estado);
                        session.save(turno);
                        request.setAttribute("mensaje", "El turno fue asignado");
                    } else {
                        request.setAttribute("mensaje", "Horario no disponible");
                    }
                    
                } else {
                    request.setAttribute("mensaje", mensaje);
                }
                session.getTransaction().commit();
                session.close();
            }
            pagina = rutaPaginaUtil.getRutaPagina("altaTurno");
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        }   catch (ParseException ex) {
            Logger.getLogger(ControladorAltaTurno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
