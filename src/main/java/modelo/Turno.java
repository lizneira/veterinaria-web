package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "turnos", catalog = "veterinaria", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Turno.buscarTodo", query = "SELECT t FROM Turno t")
    , @NamedQuery(name = "Turno.buscarTurnoID", query = "SELECT t FROM Turno t WHERE t.turnoID = :turnoID")
    , @NamedQuery(name = "Turno.buscarNombreDuenio", query = "SELECT t FROM Turno t WHERE t.nombreDuenio = :nombreDuenio")
    , @NamedQuery(name = "Turno.buscarPorNumeroTelefono", query = "SELECT t FROM Turno t WHERE t.numeroTelefono = :numeroTelefono")
    , @NamedQuery(name = "Turno.buscarNombreMascota", query = "SELECT t FROM Turno t WHERE t.nombreMascota = :nombreMascota ")
    , @NamedQuery(name = "Turno.buscarTipo", query = "SELECT t FROM Turno t WHERE t.tipo = :tipo")
    , @NamedQuery(name = "Turno.buscarTiempo", query = "SELECT t FROM Turno t WHERE t.tiempo = :tiempo")
    , @NamedQuery(name = "Turno.buscarEstado", query = "SELECT t FROM Turno t WHERE t.estado = :estado")})
public class Turno implements Serializable {

 
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "turnoID", nullable = false)
    private Integer turnoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombreDuenio", nullable = false, length = 30)
    private String nombreDuenio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numeroTelefono", nullable = false)
    private int numeroTelefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombreMascota", nullable = false, length = 30)
    private String nombreMascota;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "tipo", nullable = false, length = 30)
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tiempo", nullable = false)
    @Temporal(TemporalType.TIME)
    private Date tiempo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "estado", nullable = false, length = 30)
    private String estado;

    public Turno() {
    }

    public Turno(Integer turnoID) {
        this.turnoID = turnoID;
    }

    public Turno(Integer turnoID, String nombreDuenio, int numeroTelefono, String nombreMascota, String tipo, Date tiempo, String estado) {
        this.turnoID  = turnoID ;
        this.nombreDuenio= nombreDuenio;
        this.numeroTelefono = numeroTelefono;
        this.nombreMascota = nombreMascota;
        this.tipo = tipo;
        this.tiempo = tiempo;
        this.estado = estado;
    }

    public Integer getTurnoID() {
        return turnoID;
    }

    public void setTurnoID(Integer turnoID) {
        this.turnoID = turnoID;
    }

    public String getNombreDuenio() {
        return nombreDuenio;
    }

    public void setNombreDuenio(String nombreDuenio) {
        this.nombreDuenio = nombreDuenio;
    }

    public int getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(int numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getNombreMascota() {
        return nombreMascota;
    }

    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getTiempo() {
        return tiempo;
    }

    public void setTiempo(Date tiempo) {
        this.tiempo = tiempo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (turnoID != null ? turnoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Turno)) {
            return false;
        }
        Turno otro = (Turno) object;
        if ((this.turnoID == null && otro.turnoID != null) || (this.turnoID != null && !this.turnoID.equals(otro.turnoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Turnos [ turnoID=" + turnoID + " ]";
    }

}
