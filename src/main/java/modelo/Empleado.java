/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "empleados", catalog = "veterinaria", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.buscarTodo", query = "SELECT e FROM Empleado e")
    , @NamedQuery(name = "Empleado.buscarEmpleadoID", query = "SELECT e FROM Empleado e WHERE e.empleadoID = :empleadoID")
    , @NamedQuery(name = "Empleado.buscarNombreEmpleado", query = "SELECT e FROM Empleado e WHERE e.nombreEmpleado = :nombreEmpleado")
    , @NamedQuery(name = "Empleado.buscarContraseniaEmpleado", query = "SELECT e FROM Empleado e WHERE e.contraseniaEmpleado= :contraseniaEmpleado")
    , @NamedQuery(name = "Empleado.buscarRolEmpleado", query = "SELECT e FROM Empleado e WHERE e.rolEmpleado = :rolEmpleado ")})
public class Empleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "empleadoID")
    private Integer empleadoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombreEmpleado")
    private String nombreEmpleado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "contraseniaEmpleado")
    private String contraseniaEmpleado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "rolEmpleado")
    private String rolEmpleado;

    public Empleado() {
    }

    public Empleado(Integer empleadoID) {
        this.empleadoID = empleadoID;
    }

    public Empleado(Integer empleadoID, String nombreEmpleado, String contraseniaEmpleado, String rolEmpleado) {
        this.empleadoID = empleadoID;
        this.nombreEmpleado = nombreEmpleado;
        this.contraseniaEmpleado = contraseniaEmpleado;
        this.rolEmpleado = rolEmpleado;
    }

    public Integer getEmpleadoID() {
        return empleadoID;
    }

    public void setEmpleadoID(Integer empleadoID) {
        this.empleadoID = empleadoID;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getContraseniaEmpleado() {
        return contraseniaEmpleado;
    }

    public void setContraseniaEmpleado(String contraseniaEmpleado) {
        this.contraseniaEmpleado = contraseniaEmpleado;
    }

    public String getRolEmpleado() {
        return rolEmpleado;
    }

    public void setRolEmpleado(String rolEmpleado) {
        this.rolEmpleado = rolEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empleadoID != null ? empleadoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado otro = (Empleado) object;
        if ((this.empleadoID == null && otro.empleadoID != null) || (this.empleadoID != null && !this.empleadoID.equals(otro.empleadoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Empleados[ empleadoID=" + empleadoID + " ]";
    }
    
}
