package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "productos", catalog = "veterinaria", schema = "")
@NamedQueries({
    @NamedQuery(name = "Producto.buscarTodo", query = "SELECT p FROM Producto p")
    , @NamedQuery(name = "Producto.buscarProductoID", query = "SELECT p FROM Producto p WHERE p.productoID = :productoID")
    , @NamedQuery(name = "Producto.buscarDescripcion", query = "SELECT p FROM Producto p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Producto.buscarTipo", query = "SELECT p FROM Producto p WHERE p.tipo = :tipo")
    , @NamedQuery(name = "Producto.buscarPrecio", query = "SELECT p FROM Producto p WHERE p.precio = :precio")
    , @NamedQuery(name = "Producto.buscarEstado", query = "SELECT p FROM Producto p WHERE p.estado = :estado")})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "productoID", nullable = false)
    private Integer productoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "descripcion", nullable = false, length = 30)
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "tipo", nullable = false, length = 30)
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio", nullable = false)
    private int precio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "estado", nullable = false, length = 30)
    private String estado;

    public Producto() {
    }

    public Producto(Integer productoID) {
        this.productoID = productoID;
    }

    public Producto(Integer productoID, String descripcion, String tipo, int precio, String estado) {
        this.productoID = productoID;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.precio = precio;
        this.estado = estado;
    }

    public Integer getProductoID() {
        return productoID;
    }

    public void setProductoID(Integer productoID) {
        this.productoID = productoID;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoID != null ? productoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto otro = (Producto) object;
        if ((this.productoID == null && otro.productoID != null) || (this.productoID != null && !this.productoID.equals(otro.productoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Productos[ productoID=" + productoID + " ]";
    }
    
}
