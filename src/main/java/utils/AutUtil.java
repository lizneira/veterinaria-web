package utils;

import java.util.List;
import modelo.Empleado;
import modelo.Producto;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class AutUtil {
    
    List <Empleado> empleados = null;
    List <Producto> productos = null;
    
    public List<Empleado> getEmpleado(Session session, String nombreEmpleado, String contraseniaEmpleado) {     
        String hql = "FROM Empleado e WHERE e.nombreEmpleado = :en AND e.contraseniaEmpleado = :ep";
        Query query = session.createQuery(hql);
        query.setParameter("en", nombreEmpleado);
        query.setParameter("ep", contraseniaEmpleado);
        empleados = query.getResultList();
        return empleados;
    }
    
    public boolean hayProducto (Session session, String descripcion, String tipo, int precio, String estado) {
        String hql = "FROM Producto p WHERE p.descripcion  = :de AND p.tipo = :ti AND p.precio = :pr AND p.estado = :st";
        Query query = session.createQuery(hql);
        query.setParameter("de", descripcion);
        query.setParameter("ti", tipo);
        query.setParameter("pr", precio);
        query.setParameter("st", estado);
        productos = query.getResultList();
        boolean hayProducto = !productos.isEmpty();
        return hayProducto;
    }
    
}
