package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Turno;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class TurnoUtil {
    
    List <Turno> turnos = null;
    public List<Turno> getTurnos(Session session, String estado) {
        String hql;
        hql = "FROM Turno t WHERE t.estado = :st";
        Query query = session.createQuery(hql);
        query.setParameter("st", estado);
        turnos = query.getResultList();
        return turnos;
    }
    
    public int eliminarTurno(Session session, String[] turnoIDs, String estado) {
        int numeroRegistroEliminado = 0;
        String hql = "UPDATE Turno t SET estado = :st WHERE  t.turnoID = :ti";
        Query query = session.createQuery(hql);
        for(String turnoID : turnoIDs) {
            query.setParameter("st", estado);
            query.setParameter("ti", Integer.parseInt(turnoID));
            numeroRegistroEliminado = numeroRegistroEliminado + query.executeUpdate();
        }
        return numeroRegistroEliminado;
    }
    
    public String obtenerMensajeValidacionTiempo(String tiempo) {
        /*Los turnos se dan desde las 9hs hasta las 18hs en
         intervalos de 30 minutos y con un receso de 12hs a 13.30hs*/
         /* 9 a 12 y de 13:30 18 */
         /* 9, 9:30, ...*/
        String respuesta = "OK";
        try {
            SimpleDateFormat formatoTiempo = new SimpleDateFormat("HH:mm");
            Date horaInicioPrimerBloque = formatoTiempo.parse("9:00");
            Date horaFinPrimerBloque = formatoTiempo.parse("12:00");
            Date horaInicioSegundoBloque = formatoTiempo.parse("13:30");
            Date horaFinSegundoBloque = formatoTiempo.parse("18:00");
            Date horaFormateada = formatoTiempo.parse(tiempo);
             
            if ( (horaInicioPrimerBloque.compareTo(horaFormateada) <= 0 && horaFinPrimerBloque.compareTo(horaFormateada) >= 0)
                ||(horaInicioSegundoBloque.compareTo(horaFormateada) <= 0 && horaFinSegundoBloque.compareTo(horaFormateada) >= 0) 
               ) {
                    long diferenciaMinutos =  (horaFormateada.getTime() - horaInicioPrimerBloque.getTime()) / 1000 / 60;
                    if (diferenciaMinutos % 30 > 0) {
                        respuesta = "Los horarios se deben asignar cada 30 minutos";
                    }
            } else {
                respuesta = "Hora sin atencion al publico";
            }
        }
        catch(ParseException e) {    
            respuesta = "La hora no es válida";
        }        
        return respuesta;
    }

    public boolean tiempoDisponible(Session session, String tiempo) {
        boolean respuesta = false;
        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
            Date horaFormateada = timeFormatter.parse(tiempo);
           String hql;
            hql = "FROM Turno t WHERE t.tiempo = :ti";
            Query query = session.createQuery(hql);
            query.setParameter("ti", horaFormateada);          
            turnos = query.getResultList();
            respuesta = turnos.isEmpty();
        } catch (ParseException ex) {
            Logger.getLogger(TurnoUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return respuesta;
    }
    
}
