package utils;

import java.util.List;
import modelo.Producto;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class ProductoUtil {
    
    List <Producto> productos = null;
    public List<Producto> getProductos(Session session, String tipoProducto, String estado) {
        String hql = "FROM Producto p WHERE  p.tipo = :tipo AND p.estado = :estado";
        Query query = session.createQuery(hql);
        query.setParameter("tipo", tipoProducto);
        query.setParameter("estado", estado);
        productos = query.getResultList();
        return productos;
    }
    
    public int eliminarProducto(Session session, String[] productoIDs, String estado) {
        int numeroRegistroEliminado = 0;
        String hql = "UPDATE Producto p SET estado = :st WHERE  p.productoID = :pi";
        Query query = session.createQuery(hql);
        for(String productoID : productoIDs) {
            query.setParameter("st", estado);
            query.setParameter("pi", Integer.parseInt(productoID));
            numeroRegistroEliminado += query.executeUpdate();
        }
        return numeroRegistroEliminado;
    }
}
