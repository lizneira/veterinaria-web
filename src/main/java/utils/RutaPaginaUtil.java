package utils;

import java.util.HashMap;
import java.util.Map;

public class RutaPaginaUtil {
    
    Map<String, String> rutaPagina = null;   
    public RutaPaginaUtil(){
        rutaPagina = new HashMap<>();	
        rutaPagina.put("recepcionista", "recepcionista.jsp");
        rutaPagina.put("veterinario", "veterinario.jsp");
        rutaPagina.put("login", "login.jsp");        
        rutaPagina.put("altaProducto", "altaProducto.jsp");
        rutaPagina.put("productoDisponible", "productoDisponible.jsp");
        rutaPagina.put("venderProducto", "venderProducto.jsp");
        rutaPagina.put("productoVendido", "productoVendido.jsp");
        rutaPagina.put("listaTurno", "listaTurno.jsp");
        rutaPagina.put("altaEmpleado", "altaEmpleado.jsp");
        rutaPagina.put("altaTurno", "altaTurno.jsp");
        rutaPagina.put("administrador", "administrador.jsp");
        rutaPagina.put("index", "index.jsp");
    }
    
    public String getRutaPagina(String key) {
      return rutaPagina.get(key);
    }
}
