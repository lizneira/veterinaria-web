<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/login.css" type="text/css"/>
        <title>Veterinario</title>
    </head>
    <body class="home-veterinario">
        <button class="button sign-out-button" onclick="location.href='login.jsp'" type="button">Cerrar Sesión</button><br>
         <div class="div-title-page">
            <h1 class="h1-title-page">CONSULTORIO</h1>
        </div>
        <div class="main">
            <div class="secondary">
                <form id="form-one" method="post" action="ControladorListaProductosVendidos">
                    <button class="button" type="submit" name="tipoProducto" value="medicamento" form="form-one">Vender medicamentos</button><br>
                    <button class="button" type="submit" name="tipoProducto" value="regular" form="form-one">Vender regulares</button>
                    <input type="hidden" name="sender" value="veterinario">
                </form>
                <form id="form-two" method="post" action="ControladorListaTurnos">
                    <button class="button" type="submit" name="estado" value="espera" form="form-two">Atender turnos</button><br>
                </form>
            </div>
        </div>
    </body>
</html>
