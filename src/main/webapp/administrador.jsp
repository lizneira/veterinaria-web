<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/login.css" type="text/css"/>
        <title>Administrador</title>
    </head>
    <body class="home-administador">
        <button class="button sign-out-button" onclick="location.href='login.jsp'" type="button">Cerrar Sesión</button><br>
        <div class="div-title-page">
            <h1 class="h1-title-page">ADMINISTRACIÓN</h1>
        </div>
        <div class="main">
            <div class="secondary">
                <button class="button" onclick="location.href='altaEmpleado.jsp'" type="button">Alta empleados</button><br>
                <button class="button" onclick="location.href='altaProducto.jsp'" type="button">Alta productos</button>
                <form id="form-one" method="post" action="ControladorProductoDisponible">
                    <button class="button" type="submit" name="tipoProducto" value="medicamento" form="form-one">Medicamentos disponibles</button><br>
                    <button class="button" type="submit" name="tipoProducto" value="regular" form="form-one">Regulares disponibles</button>
                </form>
                <form id="form-two" method="post" action="ControladorProductoVendido">
                    <button class="button" type="submit" name="tipoProducto" value="medicamento" form="form-two">Medicamentos vendidos</button><br>
                    <button class="button" type="submit" name="tipoProducto" value="regular" form="form-two">Regulares vendidos</button>
                </form>
            </div>
        </div>
    </body>
</html>
