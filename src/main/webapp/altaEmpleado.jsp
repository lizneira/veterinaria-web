<%@ taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel ="stylesheet" href="./css/estilo.css" type="text/css"/>
        <title>Alta Empleado</title>
    </head>
    <body> 
        <h1 class="registro-titulo">Registrar Empleados</h1>
        <form class="registro" id="form-one" method="post" action="ControladorRegistro">
            <div class="register-switch">
              <input type="radio" name="rolEmpleado" value="recepcionista" id="receptionist" class="register-switch-input" checked required>
              <label for="recepcionista" class="register-switch-label">Recepcionista</label>
              <input type="radio" name="rolEmpleado" value="veterinario" id="veterinary" class="register-switch-input" required>
              <label for="veterinaria" class="register-switch-label">Veterinario</label>
            </div>
            <input type="text" class="registro-input" name="nombreEmpleado" placeholder="Usuario" required>
            <input type="password" class="registro-input" name="contraseniaEmpleado" placeholder="Contraseña" required>
            <button type="submit" class="registro-button" form="form-one" name="sender" value="administrador">Registrar</button>
        </form>
        <button class="boton boton-atras" onclick="location.href='administrador.jsp'" type="button">Regresar</button><br>
        <c:if test="${not empty mensaje}">
            <script>
                if (window.history.replaceState) {
                    window.history.replaceState( null, null, window.location.href );
                }
                alert("${mensaje}");
            </script>
        </c:if>
    </body>
</html>
