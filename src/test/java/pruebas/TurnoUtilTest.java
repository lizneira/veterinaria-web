package pruebas;

import utils.TurnoUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Turno;
import modelo.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class TurnoUtilTest {
    
    static Session session = null;
    static Turno turno = null;
    static String turnoID = null;
    static String turnoEstado = null;
    static String turnoTiempo = null;
    static Integer totalTurnos = null;
    static String hora = null;
    
    public TurnoUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
            Date formattedTime = timeFormatter.parse("11:30:00");
            turno = new Turno();
            TurnoUtilTest.session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            turno.setNombreDuenio("nombre dueño test ");
            turno.setNumeroTelefono(1444456789);
            turno.setNombreMascota("nombre mascota test");
            turno.setTipo("Perro test");
            turno.setTiempo(formattedTime);
            turno.setEstado("espera");
            session.save(turno);
            String hql = "FROM Turno ORDER BY turnoID DESC";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            List<Turno> turnos = query.getResultList();
            turnoID = Integer.toString(turnos.get(0).getTurnoID());
            turnoTiempo = String.valueOf(turnos.get(0).getTiempo());
            String[] parts = turnoTiempo.split(" ");
            hora = parts[3];  
            turnoEstado = turnos.get(0).getEstado();
            totalTurnos = turnos.size();
            String hqlTwo = "FROM Turno t WHERE t.estado =:st";
            Query queryTwo = session.createQuery(hqlTwo);
            queryTwo.setParameter("st", "espera");
            List<Turno> turnoDos = queryTwo.getResultList();
            totalTurnos = turnoDos.size();
            session.getTransaction().commit();

        } catch (ParseException ex) {
            Logger.getLogger(TurnoUtilTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Test
    public void testGetTurnos() {
        System.out.println("getTurnos");
        session.beginTransaction();
        String estado = turnoEstado;
        TurnoUtil instance = new TurnoUtil();
        int expResult = totalTurnos;
        List<Turno> result = instance.getTurnos(session, estado);
        assertEquals(expResult, result.size());
    }
    
    @Test
    public void testObtenerMensajeValidacionTiempo() {
        System.out.println("Obtener Mensaje Validacion Tiempo");
        String tiempo = hora;
        TurnoUtil instance = new TurnoUtil();
        String expResult = "OK";
        String result = instance.obtenerMensajeValidacionTiempo(tiempo);
        assertEquals(expResult, result);
    }


    @Test
    public void testTiempoDisponible() {
        System.out.println("tiempo Disponible");
        String time = turnoTiempo;
        TurnoUtil instance = new TurnoUtil();
        boolean expResult = false;
        boolean result = instance.tiempoDisponible(session, time);
        assertEquals(expResult, result);
    }
    
    
    @AfterClass
    public static void afterClass() {
        String hql = "DELETE FROM Turno WHERE turnoID=:turnoID";
        Query query = session.createQuery(hql);
        query.setParameter("turnoID", Integer.parseInt(turnoID));
        int rowCount = query.executeUpdate();
        if(rowCount == 0) {
            fail("The failure test");
        }
        session.getTransaction().commit();
        session.close();
    }
    
}
