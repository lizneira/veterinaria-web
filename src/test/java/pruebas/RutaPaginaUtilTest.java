
package pruebas;

import utils.RutaPaginaUtil;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nliz
 */
public class RutaPaginaUtilTest {

    @Test
    public void testGetRutaPagina() {
        System.out.println("getRutaPagina");
        String key = "venderProducto";
        RutaPaginaUtil instance = new RutaPaginaUtil();
        String expResult = "venderProducto.jsp";
        String result = instance.getRutaPagina(key);
        assertEquals(expResult, result);
        if(!result.equals(expResult)) {
            fail("The failure test.");
        }
    }
    
}
