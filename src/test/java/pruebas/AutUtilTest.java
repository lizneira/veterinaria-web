/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import utils.AutUtil;
import java.util.List;
import modelo.Empleado;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nliz
 */
public class AutUtilTest {
    static Session session = null;
    static Producto producto = null;
    static Integer productoID = null;
    static String descripcion = null;
    static String tipo= null;
    static String estado = null;
    static Integer precio = null;
    static Empleado empleado = null;
    static Integer empleadoID = null;
    static String nombreEmpleado = null;
    static String contraseniaEmpleado = null;
    static Integer totalEmpleados = null;
    
    public AutUtilTest() {
        this.session = HibernateUtil.getSessionFactory().openSession(); 
    }
    
    @BeforeClass
    public static void setUpClass() {
        
          session = HibernateUtil.getSessionFactory().openSession();    
    //Producto    
        producto = new Producto();
        session.beginTransaction();
        producto.setDescripcion(" producto test ");
        producto.setTipo("regular");
        producto.setPrecio(50);
        producto.setEstado("disponible");
        session.save(producto);
        
        String hqlProducto = "FROM Producto ORDER BY productoID DESC";
        Query query = session.createQuery(hqlProducto);
        query.setMaxResults(1);
        List<Producto> productos = query.getResultList();
        session.getTransaction().commit();
        productoID = productos.get(0).getProductoID();
        descripcion = productos.get(0).getDescripcion();
        tipo = productos.get(0).getTipo();
        estado = productos.get(0).getEstado();
        precio= productos.get(0).getPrecio();
    
    //Empleados
        empleado = new Empleado();
        empleado.setNombreEmpleado("empleado test");
        empleado.setContraseniaEmpleado("empleado test");
        empleado.setRolEmpleado("rol test");
        session.save(empleado);
        
        String hqlEmpleado = "FROM Empleado ORDER BY empleadoID DESC";
        Query queryEmpleado = session.createQuery(hqlEmpleado);
        queryEmpleado.setMaxResults(1);
        List<Empleado> empleados = queryEmpleado.getResultList();
        totalEmpleados = empleados.size();
        empleadoID = empleados.get(0).getEmpleadoID();
        nombreEmpleado = empleados.get(0).getNombreEmpleado();
        contraseniaEmpleado = empleados.get(0).getContraseniaEmpleado();
        
    }
    
    
    @Test
    public void testGetEmpleado() {
        System.out.println("getEmpleado");
        String nombre = nombreEmpleado;
        String contrasenia = contraseniaEmpleado;
        AutUtil instance = new AutUtil();
        int expResult = totalEmpleados;
        List<Empleado> result = instance.getEmpleado(session, nombre, contrasenia);
        assertEquals(expResult, result.size());
    }

    @Test
    public void testHayProducto() {
        System.out.println("hayProducto");
        session.beginTransaction();
        String descripcionTest = descripcion;
        String tipoTest = tipo;
        int precioTest = precio;
        String estadoTest = estado;
        AutUtil instance = new AutUtil();
        boolean expResult = true;
        boolean result = instance.hayProducto(session, descripcionTest, tipoTest, precioTest, estadoTest);
        assertEquals(expResult, result);
        if(!result) {
            fail("The failure test.");
        }
        session.getTransaction().commit();
    }
    
      @AfterClass
    public static void afterClass() {
        session.beginTransaction();
        //Producto
        String hqlProducto = "DELETE FROM Producto WHERE productoID=:productoID";
        Query queryProducto = session.createQuery(hqlProducto);
        queryProducto.setParameter("productoID", productoID);
        int rowCount = queryProducto.executeUpdate();
        if(rowCount == 0) {
            fail("The failure test");
        }
        
        //Empleados
        String hqlEmpleado = "DELETE FROM Empleado WHERE empleadoID=:empleadoID";
        Query queryEmpleado = session.createQuery(hqlEmpleado);
        queryEmpleado.setParameter("empleadoID", empleadoID);
        int rowCountEmpleado = queryEmpleado.executeUpdate();
        if(rowCountEmpleado == 0) {
            fail("The failure test");
        }
        session.getTransaction().commit();
        session.close();  
    }
    
}
