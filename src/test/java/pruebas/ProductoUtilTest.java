/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import java.util.List;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.ProductoUtil;
import static org.junit.Assert.*;

/**
 *
 * @author nliz
 */
public class ProductoUtilTest {
    
    static Session  session = null;
    static Producto producto = null;
    static String productoID = null;
    static String productoTipo = null;
    static String productoEstado = null;
    static Integer totalProductos = null;
    
    public ProductoUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
      ProductoUtilTest.session = HibernateUtil.getSessionFactory().openSession();    
        producto = new Producto();
        session.beginTransaction();
        producto.setDescripcion("test producto");
        producto.setTipo("regular");
        producto.setPrecio(20);
        producto.setEstado("disponible");
        session.save(producto);
        String hql = "FROM Producto ORDER BY productoID DESC";
        Query query = session.createQuery(hql);
        query.setMaxResults(1);
        List<Producto> productos = query.getResultList();
        productoID = Integer.toString(productos.get(0).getProductoID());
        productoTipo = productos.get(0).getTipo();
        productoEstado = productos.get(0).getEstado();
        totalProductos = productos.size();
        session.getTransaction().commit();  
        
    }
    
    @Test
    public void testEliminarProducto() {
        System.out.println("eliminarProducto");
        session.beginTransaction();
        String[] productoIDs = { productoID };
        String estado = "vendido";
        ProductoUtil instance = new ProductoUtil();
        int expResult = 1;
        int result = instance.eliminarProducto(session, productoIDs, estado);
        assertEquals(expResult, result);
        session.getTransaction().commit();
    }
    
     @AfterClass
    public static void afterClass() {
        Integer id = Integer.valueOf(productoID);
        session.beginTransaction();
        String hql = "DELETE FROM Producto WHERE productoID=:productoID";
        Query query = session.createQuery(hql);
        query.setParameter("productoID", id);
        int rowCount = query.executeUpdate();
        if(rowCount == 0) {
            fail("The failure test");
        }
        session.getTransaction().commit();
        session.close();  
    }
    
}
